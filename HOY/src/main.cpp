//Sensor PIR
#include <Arduino.h>

#define PIR 2
#define LED 6

void Cambio (void);

bool estado = 0;

void setup() 
{
  pinMode (LED, OUTPUT);
  pinMode (PIR, INPUT);
  attachInterrupt(digitalPinToInterrupt(PIR), Cambio, CHANGE);
}

void loop()
{
  if (estado = 1) 
  {
    digitalWrite (LED, 1);
  }

  else 
  {
    digitalWrite (LED, 0);
  }
}

void Cambio() 
{
  estado = !estado;
}